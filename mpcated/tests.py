import unittest

from pyramid import testing


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_home(self):
        from .views import home
        request = testing.DummyRequest()
        info = home(request)
        self.assertEqual(info['project'], 'mpcated')

    def test_receive(self):
        from .views import Tune
        request = testing.DummyRequest()

        t = Tune(request)
        ping = t.ping()
        self.assertEqual(ping['status'],'ok')
        # self.assertEqual(t.receive(), t)


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from mpcated import main
        app = main({})
        from webtest import TestApp
        self.testapp = TestApp(app)

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'project' in res.body)
        payload ={'content':'123', 'type': 'song'}
        r1=self.testapp.post_json('/tune', payload)
        self.assertEqual('mismatch' in r1.body)
        pass
