from pyramid.view import view_config,view_defaults
#from .utils import mp3 as MP3

from .structure import (
    #tune as T, \
    Tune as tune
)

@view_config(route_name='home', renderer="json")
def home(request):
    return {'project': 'mpcated'}



class view:
    def __init__(self, request):
        self.request = request


@view_defaults(renderer='json', route_name='tune')
class Tune(view):

    def __init__(self, request):
        from . import conf
        self.request = request
        self.limit = conf.max_content_length
        self.series: str = ''
        self.name = ''

    def receive(self):
        data = self.request.json #-> must:json
        type = data['type']
        if type != 'tune':
            return {'error':'type mismatch'}

        length = len(data['content'])
        if length >self.limit:
            return {'error':'over length: %d'
                            % (length - self.limit)}
        self.series = data['content'] #-> raw series
        return self

    def response(self)->dict:
        t = tune(self.series)
        content = t.generate()
        content.close()
        # path  = #old_todo: bind path with pre-config
        self.name = content.path.split('/')[-1]
        path = self.request.route_url('file',self.name)
        return {'status': 'ok', 'ctype': 'url', 'content': path}

    @view_config(request_method='POST')
    def mainResponse(self):
        self.receive()
        resp = self.response()
        resp['filename']=self.name
        resp['play'] = self.request.route_url('audio',
                                              _query={'audio':self.name})
        return resp

    # @view_config(request_method='POST',xhr=False)
    def test_resp(self):
        self.series = self.request.POST['content']
        result = self.response()
        return result


    @view_config(request_method='GET')
    def ping(self):
        return {'location':'tune', 'status':'ok',
                'limit':self.limit}

# @view_defaults(route_name='test')
class sendTest(view):
    @view_config(renderer='templates/test.htm')
    def page(self):
        return {'content':'ok'}

@view_defaults(route_name='audio')
class Audio(view):

    @view_config(renderer='templates/view.htm')
    def get(self):
        filename = self.request.GET['audio']
        return {'filename':filename}
