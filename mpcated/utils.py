from pydub import AudioSegment
from pydub.utils import which
from . import conf

AudioSegment.converter = which('ffmpeg')

def get_mp3(path):
    return AudioSegment.from_mp3(path)

def mp3(X:str):
    return get_mp3(conf.fsPath() + X)


def default_path(type:str,series=None):
    from datetime import datetime as d

    name = type+d.now().strftime("=%y%m%d-%H%M%S-%f=")
    return conf.fsPath()+'output/'+name+(series or '')+'.mp3'


def save_mp3(M:AudioSegment, P:str):
    from .structure import Content
    output:Content = M.export(P) #,format='mp3')
    output.src = M
    M.output = output
    output.path = P
    return output

class debug:
    def __init__(self):
        from . import conf
        conf.reg = type('Registry',(),dict(
            settings={'mpcated.storage':'../priv/fs'},
            package_name='mpcated'
        ))
        # conf.reg.settings = {'mpcated.storage':'../priv/fs'}

    def section1(self):
        """
        from mpcated.utils import debug as b
        b2=b()
        b2.section1()
        """
        from .structure import Tune
        self.T = Tune
        self.t = Tune('1231')
        self.t.merge()
        print(self.t.mlist)
        pass

    def gen(self, series):
        from requests import post
        u = 'http://fe3o4.net:6543/tune'
        r = post(u,json={'content':series,'type':'tune'})
        r.result= r.json()['content']
        return r


def preprocess(flist:[]):
    L = []
    for i in flist:
        m = get_mp3(i)
        m = m[:1080].fade_out(512)
        r = m.export(i)
        L.append((m,r))
        r.close()
    return L