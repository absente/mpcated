from .utils import AudioSegment as AS

def f1(X:str):
    return "audio_src/piano_tune_lite/" + X + '.mp3'

class Tune:

    S = 'CDEFGAB'
    main = {" ":f1('mute')}
    for n,i in enumerate(S): #-> major 3,4,5
        main[str(n+1)]  = f1(i+'3')
        main[i.lower()] = f1(i+'4')
        main[i]         = f1(i+'5')

    def __init__(self, series): #/ map
        from .utils import (mp3 as MP3,
         )
        self.series = series
        print(series)
        self.mkey = set(series) #-> music key
        self.mmap = {K:MP3(tune[K]) for K in self.mkey} #-> music map
        self.mlist = [(K,self.mmap[K]) for K in series]  # -> music list
        self.result:AS = None

    def merge(self):
        self.result = sum(list(zip(*self.mlist))[1])
        return self

    def generate(self,path=None):
        from .utils import save_mp3 as save, default_path
        return save(self.merge().result,
                    path or default_path('tune',self.series)) #-> output.src

tune = Tune.main

from io import TextIOWrapper
class Content(TextIOWrapper):
    path:str
    src:AS