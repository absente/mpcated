from pyramid.config import Configurator

class conf:
    from pyramid.registry import Registry
    reg:Registry
    max_content_length = 60

    @classmethod
    def get_settings(cls,K):
        settings = cls.reg.settings
        this = cls.reg.package_name
        return settings[this + '.' + K]

    @classmethod
    def fsPath(cls):
        return cls.get_settings('storage') + '/'

def main(global_config, **settings):
    """ This function r eturns a Pyramid WSGI application.
    """
    #todo: nginx hosts output files
    config = Configurator(settings=settings)
    #config.include('pyramid_mako')
    #fs_path = settings[config.registry.package_name+'.storage']
    conf.reg = config.registry
    print(settings)
    if 'pyramid_mako' in settings.get('pyramid.includes',[]):
        config.add_mako_renderer('.htm') #> .html for normal use
    config.add_static_view('static', 'static', cache_max_age=3600)
    file_path = '../'+conf.fsPath()+'output'
    # print(file_path)
    config.add_static_view('file',file_path,cache_max_age=3600)
    # config.add_static_view('file', conf.fsPath() + 'output', cache_max_age=3600)
    config.add_route('file','/file',static=True)
    config.add_route('home', '/')
    config.add_route('tune', '/tune')
    config.add_route('test', '/test')
    config.add_route('audio', '/play')

    config.scan()
    return config.make_wsgi_app()
